# Maven

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`3.5.4-openjdk-8-8.6`](https://gitee.com/anolis/docker-images/blob/master/maven/3.5.4-openjdk-8/8.6/Dockerfile)
- [`3.5.4-openjdk-11-8.6`](https://gitee.com/anolis/docker-images/blob/master/maven/3.5.4-openjdk-11/8.6/Dockerfile)
- [`3.5.4-openjdk-17-8.6`](https://gitee.com/anolis/docker-images/blob/master/maven/3.5.4-openjdk-17/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/maven:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage
```shell
docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven openanolis/maven mvn clean install

```

# Maven
Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.
