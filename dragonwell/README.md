# Dragonwell

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`8-8.6`, `latest`](https://gitee.com/anolis/docker-images/dragonwell/8/8.6/Dockerfile)

# Supported architectures

- arm64, amd64

# Build reference

1. Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/dragonwell:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

#Alibaba Dragonwell
Alibaba Dragonwell, as a downstream version of OpenJDK, is the in-house OpenJDK implementation at Alibaba optimized for online e-commerce, financial, logistics applications running on 100,000+ servers. Alibaba Dragonwell is the engine that runs these distributed Java applications in extreme scaling.
