#!/bin/bash
TABLES=${2:-100}
TABLE_SIZE=${3:-4000}
TIME=${4:-30}
THREADS=${5:-16}

sysbench oltp_update_non_index --db-ps-mode=auto --mysql-host=$1 --mysql-port=3001 --mysql-user=sysbench --mysql-password=password --mysql-db=sysdb --tables=$TABLES --table_size=$TABLE_SIZE --time=$TIME --report-interval=1 --threads=$THREADS prepare
sysbench oltp_update_non_index --db-ps-mode=auto --mysql-host=$1 --mysql-port=3001 --mysql-user=sysbench --mysql-password=password --mysql-db=sysdb --tables=$TABLES --table_size=$TABLE_SIZE --time=$TIME --report-interval=1 --threads=$THREADS run
sysbench oltp_update_non_index --db-ps-mode=auto --mysql-host=$1 --mysql-port=3001 --mysql-user=sysbench --mysql-password=password --mysql-db=sysdb --tables=$TABLES --table_size=$TABLE_SIZE --time=$TIME --report-interval=1 --threads=$THREADS cleanup


