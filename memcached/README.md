# Memcached

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`1.5.22-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/memcached/1.5.22/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/memcached:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage
```shell
docker run --name my-memcache -d openanolis/memcached
```

# Memcached
Memcached is a general-purpose distributed memory caching system. It is often used to speed up dynamic database-driven websites by caching data and objects in RAM to reduce the number of times an external data source (such as a database or API) must be read.
