Asynch Mode for NGINX\*

## Introduction

Nginx\* [engine x] is an HTTP and reverse proxy server, a mail proxy server,
and a generic TCP/UDP proxy server, originally written by Igor Sysoev.
Asynch mode for Nginx provides an extended Nginx working with asynchronous mode OpenSSL\*.
With Intel&reg; QuickAssist Technology (QAT) acceleration, Asynch Mode for NGINX\*
can provide significant performance improvement. For more details please, see this [asynch_mode_nginx](https://github.com/intel/asynch_mode_nginx) project.

Asynch_mode_nginx has been integrated in Anolis OS, and you can see more details from this [best practice document](https://openanolis.cn/sig/crypto/doc/390714951012679780).

This image is based on Anolis OS 8.6 and asynch_mode_nginx, it is an easier way for users to experience the performance improvements it brings.
