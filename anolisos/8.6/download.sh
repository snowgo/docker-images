#! /bin/bash

ARCHS="aarch64 x86_64"
VERSIONS=${1:-"8.6"}
for ARCH in $ARCHS
do
	if [ $ARCH == "aarch64" ]
	then
        	TARGETARCH=arm64
        elif [ $ARCH == "x86_64" ]
	then
		TARGETARCH=amd64
	else
		echo "Unsupported arch: ${$ARCH}"
		exit 1
	fi
	for VERSION in $VERSIONS
	do
		IMAGE=AnolisOS-$VERSION-$ARCH-docker.tar
		TARGET=AnolisOS-rootfs-$TARGETARCH.tar
		wget https://mirrors.openanolis.cn/anolis/$VERSION/isos/GA/$ARCH/$IMAGE
		tar -xf $IMAGE --wildcards "*/layer.tar" --strip-components 1
		rm $IMAGE
		mv layer.tar $TARGET
		xz -T0 -z $TARGET
	done
done
sed -i "s/\(org.opencontainers.image.created=\"\).*\(\" \\)/\1$(date +"%F %T%z")\2/g" Dockerfile
